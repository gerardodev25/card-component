import React from 'react';

import styled from 'styled-components';

const Container = styled.section`
  font-family: Arial, Helvetica, sans-serif;
  font-size: 14px;
  box-sizing: border-box;
  display: flex;
  justify-content: space-between;
  padding: 1.5rem;
  height: 280px;
  width: 560px;
  flex-wrap: wrap;

  @media screen and (max-width: 560px) {
    flex-direction: column;
    height: 560px;
    width: 280px;
  }
`;

const Img = styled.img`
  height: 100%;
  width: 47%;
  object-fit: cover;
  display: block;
  border-radius: 0.5rem;

  @media screen and (max-width: 560px) {
    height: 45%;
    width: 100%;
  }
`;

const RightContainer = styled.div`
  width: 47%;
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: space-between;

  @media screen and (max-width: 560px) {
    height: 49%;
    width: 100%;
  }
`;

const TextContainer = styled.div``;

const Title = styled.h2`
  font-size: 1.5rem;
  margin-top: 0;
  margin-bottom: 0.7rem;
`;
const Paragrahp = styled.p`
  line-height: 1.3rem;
`;

const Buuton = styled.button`
  height: 2.5rem;
  border-radius: 0.6rem;
  background-color: #2154cc;
  color: #e2eaf8;
  font-size: 1.1rem;
  cursor: pointer;
  outline: none;
  border: 0;
  :hover {
    background-color: #3462ce;
  }
  transition: background-color .15s ease-in;
`;

const App = ({ title, img, text, textButton }) => {
  return (
    <div>
      <Container>
        <Img src={img} alt={img} />
        <RightContainer>
          <TextContainer>
            <Title>{title}</Title>
            <Paragrahp>{text}</Paragrahp>
          </TextContainer>
          <Buuton>{textButton}</Buuton>
        </RightContainer>
      </Container>
    </div>
  );
};
export default App;
