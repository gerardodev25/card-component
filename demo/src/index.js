import React, { Component } from 'react';
import { render } from 'react-dom';

import 'normalize.css';

import navidad from '../../src/assets/images/navida.jpg';
import party from '../../src/assets/images/party.jpg';

import App from '../../src';

export default class Demo extends Component {
  render() {
    const textButton = 'nulla pariatur.';
    const title = 'Lorem ipsum dolor sit';
    const text =
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis.';
    return (
      <div>
        <h1>card-component Demo insert mendoza</h1>
        <App
          title={title}
          img={navidad}
          text={text}
          textButton={textButton}
        />
        <App
          title={title}
          img={party}
          text={text}
          textButton={textButton}
        />
      </div>
    );
  }
}

render(<Demo />, document.querySelector('#demo'));
